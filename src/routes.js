const express = require('express');

const routes = express.Router();

const userController = require('./app/controllers/UserController');

routes.get('/users', userController.index);
routes.get('/users/:id', userController.searchFor);
routes.post('/users', userController.store);
routes.delete('/users/:id', userController.destroy);
routes.put('/users/:id', userController.updateUser);

module.exports = routes;
