require('dotenv/config');

module.exports = {
  username: process.env.USRNAME,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  host: process.env.HOST,
  dialect: process.env.DIALECT,
  define: {
    timestamps: true,
    underscored: true,
  },
};
