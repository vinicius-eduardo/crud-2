const Yup = require('yup');
const User = require('../models/User');

module.exports = {
  async index(req, res) {
    const {
      page,
    } = req.query;
    const limit = 10;
    const offset = limit * (page - 1);

    const users = await User.findAll({
      attributes: ['id', 'name', 'email', 'created_at'],
      order: ['id'],
      limit,
      offset,
    });

    if (users.length === 0) {
      if (offset > 0) {
        return res.status(404).send('Você chegou ao fim da lista');
      }
      return res.status(404).send('Ainda não existe usuários');
    }

    return res.json(users);
  },
  async searchFor(req, res) {
    const user = await User.findByPk(req.params.id, {
      attributes: ['id', 'name', 'email', 'created_at'],
    });

    if (!user) {
      res.status(404).send('Usuário não encontrado.');
    }

    return res.json(user);
  },
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      password: Yup.string().required(),
      confirmPassword: Yup.string().oneOf([Yup.ref('password')]).required(),
    });

    const validated = await schema.isValid(req.body);

    if (!validated) {
      return res.status(400).send('Requisição inválida');
    }

    const findUser = await User.findOne({
      where: {
        email: req.body.email,
      },
    });

    if (findUser) {
      res.status(400).send('Este email já foi cadastrado');
    }

    const {
      name,
      email,
      password,
    } = req.body;

    const result = await User.create({
      name,
      email,
      password,
    });

    if (result) {
      return res.status(200).json({
        name,
        email,
      });
    }
    return res.status(400);
  },
  async destroy(req, res) {
    const result = await User.destroy({
      where: {
        id: req.params.id,
      },
    });

    if (result) {
      return res.send('destruido');
    }
    return res.send('não destruido');
  },
  async updateUser(req, res) {
    // esta parte criam o shape com confirmPassord apenas se o passowrd estiver presente
    let param = {
      name: Yup.string(),
      email: Yup.string().email(),
    };

    if (req.body.password) {
      param = {
        ...param,
        password: Yup.string(),
        confirmPassword: Yup.string()
          .oneOf([Yup.ref('password')])
          .required(),
      };
    }

    const schema = Yup.object().shape(param);

    const validated = await schema.isValid(req.body);

    if (!validated) {
      return res.status(400).send('Requisição inválida');
    }

    const user = await User.findByPk(req.params.id, {
      attributes: ['password'],
    });

    if (!user) {
      res.status(404).send('Usuário não encontrado.');
    }

    if (req.body.email !== user.email) {
      const invalidMail = await User.findOne({
        where: {
          email: req.body.email,
        },
      });

      if (invalidMail) {
        return res.status(400).send('Email já foi utilizado.');
      }
    }

    if (req.body.password) {
      if (req.body.password === user.password) {
        return res
          .status(400)
          .send('Sua senha deve ser diferente da senha antiga');
      }
    }

    const result = await User.update(req.body, {
      where: {
        id: req.params.id,
      },
    });

    if (result) {
      return res.status(200).send('Dados atualizados');
    }
    return res.status(400).send('Os dados não foram atualizados');
  },
};
