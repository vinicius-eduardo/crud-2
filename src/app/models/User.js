const {
  Model,
  DataTypes,
} = require('sequelize');

class User extends Model {
  static init(conenction) {
    super.init({
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    }, {
      sequelize: conenction,
    });
  }
}

module.exports = User;
